# Projet android : PokeBuilder

Dans le cadre de notre formation au sein de l'IUT en Licence Pro web et mobilité, Nicolas Falguieres et Huré Laura avons réalisé une application android sur le thème des pokemon.

## Installation

Vous trouverez l'apk versionné à la racine du projet (*pokebuilder.apk*). Il vous est également possible de le générer
vous même à partir d'android studio en buildant le projet. Vous trouverez ensuite le fichier
.apk dans le dossier  *PokeBuilder/app/build/output/debug/app-debug.apk*.

Dans tous les cas vous n'avez cas transférer le fichier sur votre téléphone, puis l'installer.

## Environnements de Test et libréries

Les tests ont été effectués sur nos appareils personnels, version Nougat (7.0) et Oreo (8.0) d'android. **AUCUN TESTS** n'a été effectué sur une **TABLETTE**, nous ne garantisson donc pas le bon
fonctionnement de l'application sur ce support.

Les libréries utilisé dans ce projet sont les suivantes:
- pokekotlin:2.3.0 : pour la pokeAPI
- jsoup-1.11.2 : pour faire du scrapping (un jar ajouter à la main)
- glide:3.6.1 : pour le download et l'affichage d'image
- guava:24.0-android : pour la conversion de certaines données

## PokeBuilder

PokeBuilder est axé sur le monde de pokemon, un jeu qui propose au joueur de parcourir le monde
en capturant et élevant pour le combat des petits monstres (les pokemon). Dans le jeux le joueur ne peut transporter que 6 pokemon à la fois, c'est se que l'on appelle son équipe.

Dans l'application les pokemon sont appelés des "sets stratégiques". Chaque set appartiens à une
catégorie particulière (un tier).

Pokebuilder est une application qui permet de créer et enregistrer des équipes personnalisées. L'utilisateur à la possibilité d'aller chercher des sets stratégiques  en précisant le tier souhaité en recherche. Il peut ensuite ajouter le pokemon sélectionné à une équipe préàlablement crée. Il est également possible de consulter la liste des objets de pokemon.

## Gestion des équipes
Lorsque l'application s'ouvre l'utilisateur est directement dans l'onglet *"Vos equipes"*. Dans cette onglet il est possible de créer des équipes en cliquant sur le bouton *"+"* en bas à droite de l'écran. Une pop-up s'ouvrira demandant le nom de l'équipe à ajouter.

Le nom de l'équipe ne doit pas être vide ou exéder 20 caractères. Les espaces inutile en début et
fin sont supprimer, et les retour à la ligne sont remplacer par des espaces.

Il est possible de supprimer une équipe en cliquant sur le bouton supprimer. Une pop-up de confirmation s'ouvrira alors.

Les équipes et leurs contenus sont stockés dans une base de données local. Désinstaller l'application inclut la perte de vos données.

Pour chaque équipe les informations suivantes sont affichées :
	- le nom de l'équipe
	- le tier *(qui correspond au tier du premier pokemon ajouté)*
	- la description des pokemons de l'équipe *(dans la partie déroulante)*.

Les équipes sont affichées dans un *RecyclerView* et chaque objet de la liste est une *CardView*.

## Set stratégiques
Les sets stratégiques permettent à l'utilisateur d'aller chercher des sets préfait  du tier souhaité et d'ajouter un set sélectionné à l'équipe souhaitée. Lorsque l'utilisateur clic sur l'onglet "set stratégique", une pop-up demandant le *tier* recherché s'ouvre.
L'application va ensuite lui afficher dans un *RecyclerView* avec des *CardView*, tous les sets de pokemon trouvés appartenant à ce *tier*.

Pour chaque set de pokemon les informations sont affichées :
	- Le nom français et anglais *(entre parenthèse)*
	- Les attaques
	- Les détails
	- Les statistiques
	- Le texte de partage pour showdown

Pour ajouter le set du pokemon à une équipe, il faut cliquer sur le bouton "Ajouter". Une pop-up avec la liste des équipes s'affiche alors. Seul les équipes pouvant encore acceuillir des pokemon
sont proposé par la pop-up.

Les données des sets stratégiques sont scrappées sur le site [pokestrat] (http://pokestrat.com/set-strategique/recherche).

## Les objets
Les objets sont uniquement accessible pour la consultation. La liste est une *ListView* avec des *customs* items. Chaque objet affiche l'information suivante: le *nom de l'objet* en anglais.

Les données des objets sont fournies par l'AP  [pokeAPI] (https://pokeapi.co/), qui distribue des données sur les pokemons (principalement en anglais). La pokeAPI limite le nombre de connections à l'API pour une addresse IP donné à 300.

### recherche d'objets par leurs noms
Lorsque l'utilisateur est dans l'onglet "objets" il peut effectuer une recherche en cliquant sur l'icone de recherche du menu. La recherche prend uniquement le nom de l'objet en anglais en paramètre, il est possible d'entrer autre chose mais celà ne trouvera pas forcément de résultats
pertinent.
Cette dernière n'est pas très performante. En effet nous nous contentons de chercher un mot dont le début ou la totalité correspond à la recherche. Les résultats sont rendus non triés pour des soucis de rapidité.

## Les options
En cliquant sur les 3 "." de la barre de menu, on peut accéder aux options. Les options permettent de paramétrer l'application. Pour l'instant il n'y à qu'un seul parametre utile à l'application qui est l'addresse mail utilisateur. L'utilisateur peut entrer son adresse personnelle. Elle sera ensuite utiliser lors de l'envois de mail.

L'addresse mail est stockée dans ce que l'on appelle les *préférences*.

## Envois de mail
L'utilisateur peut envoyer ses équipes *(et set associés)* par mail. L'adresse mail de destination est celle renseignée dans les options de l'application.

## Gestion de l'état de la connexion
L'application écoute les changement d'états de la connexion de l'utilisateur et désactive certaines options (*Set stratégique* et *Objet*) lorsque l'utilisateur n'a pas de connexion internet. Ces états sont re-activé lorsque la connexion est rétablie.

## Organisation de l'application
L'application possède une *Activité* principale *(pokebuilder)* qui possède le menu de navigation
et un contenu central initialement vide. Lors du lancement de l'application on charge automatiquement l'onglet *"Vos équipes"*. Chaque onglet est en réalité un fragment qui va prendre
la place du contenu central de l'activité mère.

Les fragments *"Vos équipes"* et *"Set stratégique"* sont des *RecyclerView* avec des *CardView* en tant qu'item de la liste.

Le fragment *"Objets"* est une *listView* avec un *adapter custom* permettant d'afficher des items plus complexe qu'un simple texte.

Cependant, les options sont une tout autre activité. L'activité *"Settings"* possède sa propre *action bar*. Les options sont eux dans un *fragment* qui prend la place du contenu initialement vide de l'activité *"Settings mère"*.

Certaines informations *(les objets/entity représentant les données de la base version POO)*, devaient transiter au sein de l'application et être passé à un fragment, une fenêtre de dialog ou une activité via des bundle. Un *bundle* est ce qui permet de faire transiter des informations entre les activités, les fragments,... Pour celà tous les objets dans le dossier *"Entity"* on été rendu [Parcelable] (http://www.tutos-android.com/parcelable-android) car un objet complexe doit être *"Parcelable"* pour être utilisé dans un Bundle.

La recherche dans l'onglet *"Objets"* est un [SearchDialog] (https://developer.android.com/guide/topics/search/search-dialog.html).


## Organisation de la base de données
Pour chaque table de la base de données une classe représentant cette table existe dans le dossier *"Entity"* de l'application. Ce sont ces *entitys* qui sont utilisées ensuite dans l'application.

Pour communiquer et générer les entitys avec les données requetées en base, nous avons créé les classes DAO. Il y a une classe DAO pour chaque table de la base. Les DAO s'occupent des requetes CRUD (*create, read, update, delete*). Les requêtes *"read"* retourne des entitys.


## La base de données

Une **Équipe** est composé d'un *nom* de six **Sets**.

Un **Set** définie la façon de construire un des membre de la team. Cela passe par le *pokemon* choisit, les *attaques* que celui-ci aura, l' *objet* qu'il portera, sa composition d' *IV* et d' *EV*, sa *nature*, son *talent*, son *tier* (conformément au norme "smogon"), ainsi que son *export* au format showdown. Dans la base, chaque Set est identifié par un identifiant. <br> Contrairement au *Talent*, à la *nature*, a l' *export*, et au *tier* qui peuvent etre des chaine de caractère simple, les **Attaques**, **Pokemon**, **Objet**, **IV** et **EV** sont définient dans d'autre table, apportant chacune plus d'informations. <br>

>Pkmn :

Dans la base, un pokemon est identifier par un entier. On stockera aussi son *Nom*, et on donnera la possibilité de stocké le chemin d'accès vers une image le représentant.

>IV et EV :

Dans la composition d'un Set, les *IVs* et *EVs*, représentent des valeurs ajustables disposé dans les caractéristiques du pokemons. Chaque champ est un entier représentant cette valeur. On ajoute également un *id* permettant d'appeler cette composition d'IV, ou d'EV.

> Item :

Un Item *(Objet)* est repésenté par un *id* permettant de le retrouver parmis les autres. On gardera son *nom*, ainsi qu'une *description*.

> Attaque :

On stockera simplement le *nom* de l'attaque (En plus d'un *identifiant*). Il est prévue d'enrichir cette table avec des informations supplémentaire, comme la puissance, la précision, etc ...

<img src="Base_pokebuilder.jpg" alt="Base de donnée de l'application Pokebuilder"  border="10" />

Chacune de ses tables, est représenté par une classe java. Ainsi, il est facile de représenter ces informations, ou de les faire transiter à travers l'application.

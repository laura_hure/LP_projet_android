package com.example.lhure.pokebuilder.teambuilder.team;

import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.lhure.pokebuilder.DAO.DatabaseHandler;
import com.example.lhure.pokebuilder.DAO.SetsBDD;
import com.example.lhure.pokebuilder.Entity.Equipe;
import com.example.lhure.pokebuilder.Entity.Set;
import com.example.lhure.pokebuilder.R;
import com.example.lhure.pokebuilder.DAO.EquipeBDD;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Laura on 16/01/2018.
 */

public class EquipesFragment extends Fragment {

    private final String LOG_TAG = "EquipesFragment";

    private EquipeBDD equipeBDD;
    private List<Equipe> equipes;
    public ProgressBar progressBar;
    protected RecyclerView mRecyclerView;
    protected RecyclerView.LayoutManager mLayoutManager;
    protected EquipeAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recycler_equipes,container,false);
        mRecyclerView = view.findViewById(R.id.recyclerView);
        progressBar = view.findViewById(R.id.progress_bar);

        //initialisation de la liste des equipes à vide
        equipes = new ArrayList<>();

        //creation du manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        //creation de l'adapter
        adapter = new EquipeAdapter(getActivity(),equipes);
        mRecyclerView.setAdapter(adapter);



        new DownloadEquipe().execute();

        return view;
    }

    private class DownloadEquipe extends AsyncTask<Integer,Integer,Cursor>{

        @Override
        protected void onPreExecute() {

            progressBar.setVisibility(View.VISIBLE);
            adapter.clear();
        }

        @Override
        protected Cursor doInBackground(Integer... integers) {
            //creation et ouverture de la base de données
            equipeBDD = new EquipeBDD(getContext());
            equipeBDD.open();

            return equipeBDD.getAll();
        }

        @Override
        protected void onPostExecute(Cursor result) {

            progressBar.setVisibility(View.GONE);

            //si il y a des resultats on met à jours la liste
            if(result.getCount()!= 0 ) {

                //on parcours les resultats de la recherche
                while(result.moveToNext()){
                    //creation de l'objet equipe
                    Equipe equipe =  new Equipe(result.getInt(0),result.getString(1));
                    for(int i = result.getColumnIndex(DatabaseHandler.EQUIPE_SET1); i<result.getColumnIndex(DatabaseHandler.EQUIPE_SET6)+1; i++){
                        if(!result.isNull(i)){ //si le set n'est pas null on l'ajout a l'equipe

                            //communication avec la base pour recuperer le set a partir de son id
                            SetsBDD setsBDD = new SetsBDD(getContext());
                            setsBDD.open();
                            Cursor cursor = setsBDD.getSetById(result.getLong(i));

                            while(cursor.moveToNext()) {
                                //on cree un set qui contien uniquement le texte showdown et le tier que l'on veut afficher
                                Set set = new Set();
                                set.setId(cursor.getLong(cursor.getColumnIndex(DatabaseHandler.ID)));
                                set.setShowDown(cursor.getString(cursor.getColumnIndex(DatabaseHandler.SETS_EXPORT)));
                                set.setTier(cursor.getString(cursor.getColumnIndex(DatabaseHandler.SETS_TIER)));
                                equipe.getSets().add(set); //puis on l'ajoute e la liste de set de l'equipe
                            }
                            setsBDD.close();
                            cursor.close();
                        }
                    }
                    adapter.add(equipe); //ajout de l'equipe a la liste de l'adapter
                }
                result.close();

                equipeBDD.close();  //fermeure de la base
                adapter.notifyDataSetChanged(); //notification a l'adapter que la liste a change
            }else{
                Toast.makeText(getContext(),"Pas d'équipe",Toast.LENGTH_SHORT);
            }
        }
    }
}

package com.example.lhure.pokebuilder.Entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Laura on 07/01/2018.
 */

public class Pokemon implements Parcelable{

    private String text;
    private String image;
    private String nameEN;
    private long id;

    public Pokemon(String text, String image, String nameEn){
        this.text = text;
        this.image = image;
        this.nameEN = nameEn;
    }

    /* Factory Creator */
    public static final Creator<Pokemon> CREATOR = new Creator<Pokemon>() {
        @Override
        public Pokemon createFromParcel(Parcel in) {
            return new Pokemon(in);
        }

        @Override
        public Pokemon[] newArray(int size) {
            return new Pokemon[size];
        }
    };

    private Pokemon(Parcel in){
        this.id = in.readLong();

        this.text = in.readString();
        this.image = in.readString();
        this.nameEN = in.readString();
    }

    public String getText(){
        return this.text;
    }
    public void setText(String text){
        this.text = text;
    }
    public String getImage(){
        return this.image;
    }
    public String getNameEN(){
        return this.nameEN;
    }
    public long getId(){return this.id;}
    public void setId(long id){this.id=id;}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(text);
        dest.writeString(image);
        dest.writeString(nameEN);
    }
}

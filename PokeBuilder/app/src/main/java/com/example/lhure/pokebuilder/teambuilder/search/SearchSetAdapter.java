package com.example.lhure.pokebuilder.teambuilder.search;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.lhure.pokebuilder.Entity.Pokemon;
import com.example.lhure.pokebuilder.Entity.Set;
import com.example.lhure.pokebuilder.R;

import java.util.List;

/**
 * Created by Laura on 07/01/2018.
 */

public class SearchSetAdapter extends RecyclerView.Adapter<SearchSetViewHolder> {

    List<Set> list;
    private Context context;

    //ajouter un constructeur prenant en entrée une liste
    public SearchSetAdapter(Context context, List<Set> list) {
        this.list = list;
        this.context = context;
    }

    //cette fonction permet de créer les viewHolder
    //et par la même indiquer la vue à inflater (à partir des layout xml)
    @Override
    public SearchSetViewHolder onCreateViewHolder(ViewGroup viewGroup, int itemType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.pokemon_set_cards,viewGroup,false);

        //on passe le contexte au view holder pour dl les images
        SearchSetViewHolder searchSetViewHolder = new SearchSetViewHolder(view);
        searchSetViewHolder.setContext(context);

        return searchSetViewHolder;
    }

    //c'est ici que nous allons remplir notre cellule avec le texte/image de chaque MyObjects
    @Override
    public void onBindViewHolder(SearchSetViewHolder searchSetViewHolder, int position) {
        Set set = list.get(position);
        searchSetViewHolder.bind(set);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void clear(){
        list.clear();
    }

    public void add(Set set){
        list.add(set);
    }
}

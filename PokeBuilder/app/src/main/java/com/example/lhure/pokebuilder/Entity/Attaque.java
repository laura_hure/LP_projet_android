package com.example.lhure.pokebuilder.Entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Laura on 12/01/2018.
 */

public class Attaque implements Parcelable{

    private String nom;
    private long id;

    public Attaque(int id, String nom){
        this.id = id;
        this.nom = nom;
    }
    public Attaque(String nom){
        this.nom = nom;
    }

    public String getNom(){return this.nom;}
    public long getId(){return this.id;}
    public void setId(long id){this.id=id;}

    /* Factory Creator */
    public static final Creator<Attaque> CREATOR = new Creator<Attaque>() {
        @Override
        public Attaque createFromParcel(Parcel in) {
            return new Attaque(in);
        }

        @Override
        public Attaque[] newArray(int size) {
            return new Attaque[size];
        }
    };

    private Attaque(Parcel in){
        this.id = in.readLong();
        this.nom = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(nom);
    }
}

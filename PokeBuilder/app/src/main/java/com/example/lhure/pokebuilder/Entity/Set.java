package com.example.lhure.pokebuilder.Entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Laura on 13/01/2018.
 */

public class Set implements Parcelable{

    private List<Attaque> attaques;
    private Pokemon pokemon;
    private Objet objet;
    private String talent, nature, showDown;
    private Ev ev;
    private long id;
    private String tier;

    public Set(){
        //constructeur par défaut
    }

    public Set(Pokemon pokemon, Objet objet, Ev ev, String talent, String nature, String showDown){
        this.pokemon = pokemon;
        this.objet = objet;
        this.ev = ev;
        this.talent = talent;
        this.nature = nature;
        this.showDown = showDown;
        this.attaques = new ArrayList<>();
    }

    public Set(String talent, String nature, String showDown,String tier,List<Attaque> attaques,Pokemon pokemon, Objet objet, Ev ev){
        this.talent = talent;
        this.nature = nature;
        this.showDown = showDown;
        this.tier = tier;
        this.attaques = attaques;
        this.pokemon = pokemon;
        this.objet = objet;
        this.ev = ev;
    }

    protected Set(Parcel in){
        this.id = in.readLong();

        attaques = new ArrayList<>();
        in.readList(attaques,Attaque.class.getClassLoader());

        this.tier = in.readString();
        this.talent = in.readString();
        this.nature = in.readString();
        this.showDown = in.readString();

        this.pokemon = in.readParcelable(Pokemon.class.getClassLoader());
        this.objet = in.readParcelable(Objet.class.getClassLoader());
        this.ev = in.readParcelable(Ev.class.getClassLoader());
    }

    /* Factory Creator */
    public static final Creator<Set> CREATOR = new Creator<Set>() {
        @Override
        public Set createFromParcel(Parcel in) {
            return new Set(in);
        }

        @Override
        public Set[] newArray(int size) {
            return new Set[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);

        dest.writeList(attaques);

        dest.writeString(tier);
        dest.writeString(talent);
        dest.writeString(nature);
        dest.writeString(showDown);

        dest.writeParcelable(pokemon,flags);
        dest.writeParcelable(objet,flags);
        dest.writeParcelable(ev,flags);
    }

    public void setTier(String tier){this.tier = tier;}

    public List<Attaque> getAttaques(){return this.attaques;}
    public Pokemon getPokemon(){return this.pokemon;}
    public Ev getEV(){return this.ev;}
    public Objet getObjet(){return this.objet;}
    public String getTalent(){return this.talent;}
    public String getNature(){return this.nature;}
    public String getShowDown(){return this.showDown;}
    public void setShowDown(String showDown){this.showDown = showDown;}
    public long getId(){return this.id;}
    public void setId(long id){this.id=id;}
    public String getTier(){return this.tier;}
}

package com.example.lhure.pokebuilder.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.util.Log;

import com.example.lhure.pokebuilder.PokeBuilderActivity;
import com.example.lhure.pokebuilder.R;

/**
 * Created by Laura on 26/01/2018.
 */

/**
 * class qui reçoit les changements d'etat de la connexion utilisateur
 */
public class NetworkChangeReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        int status = NetworkUtil.getConnectivityStatusString(context);
        Log.e("NetworkChangeReceiver", "Sulod sa network reciever");
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(intent.getAction())) {
            if(status==NetworkUtil.NETWORK_STATUS_NOT_CONNECTED){ //si on a pas de connexion on desactive tout se qui fonctionne pas sans
                ((NavigationView)((PokeBuilderActivity)context).findViewById(R.id.nav_view)).getMenu().getItem(1).setEnabled(false); //sets
                ((NavigationView)((PokeBuilderActivity)context).findViewById(R.id.nav_view)).getMenu().getItem(2).setEnabled(false); //items
            }else{ //si on a la connexion on reactive toutes les fonctionnalités desactivé
                ((NavigationView)((PokeBuilderActivity)context).findViewById(R.id.nav_view)).getMenu().getItem(1).setEnabled(true);
                ((NavigationView)((PokeBuilderActivity)context).findViewById(R.id.nav_view)).getMenu().getItem(2).setEnabled(true);
            }

        }
    }
}

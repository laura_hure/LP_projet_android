package com.example.lhure.pokebuilder.utils;

import android.support.v4.app.DialogFragment;

import org.jetbrains.annotations.Nullable;

/**
 * Created by Laura on 18/01/2018.
 */

public interface NoticeDialogListener {
    void onDialogPositiveClick(DialogFragment dialog, String checkItem,long itemId,Object item);
    void onDialogNegativeClick(DialogFragment dialog);
}

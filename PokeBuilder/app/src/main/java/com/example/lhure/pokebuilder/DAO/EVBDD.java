package com.example.lhure.pokebuilder.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.lhure.pokebuilder.Entity.Ev;

/**
 * Created by space on 07/01/2018.
 */

public class EVBDD extends DAOBase{

/////////////////////////// Constructeurs ///////////////////////
    public EVBDD(Context ctxt){
        super(ctxt);
    }

    public long insertEv(Ev e){
        ContentValues values= new ContentValues();
        //Add values
        values.put(DatabaseHandler.EV_HP, e.getPv());
        values.put(DatabaseHandler.EV_ATK, e.getAtt());
        values.put(DatabaseHandler.EV_DEF, e.getDef());
        values.put(DatabaseHandler.EV_ATKSPE, e.getSAtt());
        values.put(DatabaseHandler.EV_DEFSPE, e.getSDef());
        values.put(DatabaseHandler.EV_VIT, e.getVit());
        Log.e("Async task debug","Début de l'insert en base");
        return db.insert(DatabaseHandler.EV, null, values);
    }
    public long updateEv(Ev e){
        ContentValues values = new ContentValues();
        //values.put(COL_ID, e.getId());
        values.put(DatabaseHandler.EV_HP, e.getPv());
        values.put(DatabaseHandler.EV_ATK, e.getAtt());
        values.put(DatabaseHandler.EV_DEF, e.getDef());
        values.put(DatabaseHandler.EV_ATKSPE, e.getSAtt());
        values.put(DatabaseHandler.EV_DEFSPE, e.getSDef());
        values.put(DatabaseHandler.EV_VIT, e.getVit());
        return db.update(DatabaseHandler.EV, values, DatabaseHandler.ID+" = "+e.getId(), null);
    }
    public long removeEv(Ev e){
        return db.delete(DatabaseHandler.EV,DatabaseHandler.ID + " = "+e.getId(), null);
    }
}

package com.example.lhure.pokebuilder.teambuilder.team;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.lhure.pokebuilder.Entity.Equipe;
import com.example.lhure.pokebuilder.R;

import java.util.List;

/**
 * Created by Laura on 23/01/2018.
 */

public class EquipeAdapter extends RecyclerView.Adapter<EquipeViewHolder> {

    List<Equipe> list;
    private Context context;
    private ViewGroup parent;
    private View view;
    private EquipeViewHolder searchSetViewHolder;

    public EquipeAdapter(Context context, List<Equipe> equipes){
        this.context = context;
        this.list = equipes;
    }



    @Override
    public EquipeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.parent = parent;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.equipe_card,parent,false);

        //on passe le contexte au view holder pour dl les images
        searchSetViewHolder = new EquipeViewHolder(view,context);

        return searchSetViewHolder;
    }

    @Override
    public void onBindViewHolder(EquipeViewHolder holder, int position) {
        Equipe equipe = list.get(position);
        holder.bind(equipe);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void clear(){
        list.clear();
    }

    public void add(Equipe equipe){
        list.add(equipe);
    }
}

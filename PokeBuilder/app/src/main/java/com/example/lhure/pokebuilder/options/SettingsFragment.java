package com.example.lhure.pokebuilder.options;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;

import com.example.lhure.pokebuilder.R;

/**
 * Created by Laura on 01/03/2018.
 */

public class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {
    public static final String KEY_PREF_EMAIL = "addr_mail";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences); //chargement des prefs via un xml

        //au chargement des preferences on affiche la valeur courante de l'addresse mail
        Preference emailPref = findPreference(KEY_PREF_EMAIL);
        emailPref.setSummary(getPreferenceManager().getSharedPreferences().getString(KEY_PREF_EMAIL, ""));
    }

    /**
     * On enregistre le listener des pref lorsque le fragment n'est plus en arrière plan
     * pour un cycle de vie
     */
    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences()
                .registerOnSharedPreferenceChangeListener(this);
    }

    /**
     * on des-enregistre le listener des preferences lorsque le fragment passe ne arrière plan
     * pour un cycle de vie plus propre
     */
    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences()
                .unregisterOnSharedPreferenceChangeListener(this);
    }

    /**
     * Lorsque les preferences changes on affiche en petit la valeur courante (uniquement pour le mail pour l'instant)
     * @param sharedPreferences
     * @param key
     */
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(KEY_PREF_EMAIL)) {
            Preference emailPref = findPreference(key);
            // Set summary to be the user-description for the selected value
            emailPref.setSummary(sharedPreferences.getString(key, ""));
        }
    }
}

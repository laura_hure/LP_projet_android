package com.example.lhure.pokebuilder.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.ContactsContract;
import android.util.Log;

import com.example.lhure.pokebuilder.Entity.Equipe;
import com.example.lhure.pokebuilder.Entity.Set;

/**
 * Created by space on 07/01/2018.
 */

public class EquipeBDD extends DAOBase{
    //---------------CONSTRUCTEUR-----------------
    public EquipeBDD(Context context){super(context);}

    //-------------------CRUD---------------------
    public long insertEquipe(Equipe e){
        ContentValues values= new ContentValues();
        //Add Values
        values.put(DatabaseHandler.EQUIPE_NOM, e.getNom());
        Log.e("EquipeBDD","Début de l'insert en base");
        return db.insert(DatabaseHandler.EQUIPE, null, values);
    }
    public long updateEquipe(long id, Set set, int number){

        ContentValues values = new ContentValues();
        switch(number){
            case 1:
                values.put(DatabaseHandler.EQUIPE_SET1,set.getId());
                break;
            case 2:
                values.put(DatabaseHandler.EQUIPE_SET2,set.getId());
                break;
            case 3:
                values.put(DatabaseHandler.EQUIPE_SET3,set.getId());
                break;
            case 4:
                values.put(DatabaseHandler.EQUIPE_SET4,set.getId());
                break;
            case 5:
                values.put(DatabaseHandler.EQUIPE_SET5,set.getId());
                break;
            case 6:
                values.put(DatabaseHandler.EQUIPE_SET6,set.getId());
                break;
        }
        return db.update(DatabaseHandler.EQUIPE, values, DatabaseHandler.ID+"=? ", new String[]{String.valueOf(id)});
    }
    public long removeEquipe(long id){
        return db.delete(DatabaseHandler.EQUIPE,DatabaseHandler.ID+ " = "+id, null);
    }

    public Cursor getAll(){
        return db.rawQuery("SELECT * from " + DatabaseHandler.EQUIPE,null);
    }

    public int getNbSets(long id){
        String query = "SELECT * from "+DatabaseHandler.EQUIPE+" where "+DatabaseHandler.ID+"=?";
        Cursor cursor = db.rawQuery(query,new String[]{String.valueOf(id)});//passage de l'id en argument de la requête

        int nbSets = 0;
        //compter le nombre de sets present
        while(cursor.moveToNext()){
            if(!cursor.isNull(cursor.getColumnIndex(DatabaseHandler.EQUIPE_SET1)))
                nbSets++;
            else
                return nbSets;

            if(!cursor.isNull(cursor.getColumnIndex(DatabaseHandler.EQUIPE_SET2)))
                nbSets++;
            else
                return nbSets;

            if(!cursor.isNull(cursor.getColumnIndex(DatabaseHandler.EQUIPE_SET3)))
                nbSets++;
            else
                return nbSets;

            if(!cursor.isNull(cursor.getColumnIndex(DatabaseHandler.EQUIPE_SET4)))
                nbSets++;
            else
                return nbSets;

            if(!cursor.isNull(cursor.getColumnIndex(DatabaseHandler.EQUIPE_SET5)))
                nbSets++;
            else
                return nbSets;

            if(!cursor.isNull(cursor.getColumnIndex(DatabaseHandler.EQUIPE_SET6)))
                nbSets++;
            else
                return nbSets;
        }
        cursor.close();
        return nbSets; //pas de set associe on retourne 0
    }

    public Cursor getEquipeById(long id){
        String sql = "SELECT * FROM " + DatabaseHandler.EQUIPE + " WHERE "+DatabaseHandler.ID+"=?";
        return  db.rawQuery(sql,new String[]{String.valueOf(id)});
    }

    /**
     * prend en argument le numéro du set, l'id du set et  l'id de l'équipe
     * et vide le champs correspondant à se set
     * @param set
     * @param id
     * @return
     */
    public long removeSet(int set, long id){
        ContentValues  contentValues = new ContentValues();

        switch (set){
            case 0:
                contentValues.putNull(DatabaseHandler.EQUIPE_SET1);
                break;
            case 1:
                contentValues.putNull(DatabaseHandler.EQUIPE_SET2);
                break;
            case 2 :
                contentValues.putNull(DatabaseHandler.EQUIPE_SET3);
                break;
            case 3:
                contentValues.putNull(DatabaseHandler.EQUIPE_SET4);
                break;
            case 4:
                contentValues.putNull(DatabaseHandler.EQUIPE_SET5);
                break;
            case 5:
                contentValues.putNull(DatabaseHandler.EQUIPE_SET6);
                break;
        }
        return db.update(DatabaseHandler.EQUIPE, contentValues,DatabaseHandler.ID + "=" + id,null);
    }
}

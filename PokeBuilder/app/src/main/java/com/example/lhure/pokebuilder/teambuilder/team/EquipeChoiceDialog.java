package com.example.lhure.pokebuilder.teambuilder.team;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.res.TypedArrayUtils;
import android.widget.ListView;

import com.example.lhure.pokebuilder.DAO.DatabaseHandler;
import com.example.lhure.pokebuilder.DAO.EquipeBDD;
import com.example.lhure.pokebuilder.Entity.Set;
import com.example.lhure.pokebuilder.R;
import com.example.lhure.pokebuilder.utils.NoticeDialogListener;
import com.google.common.base.Strings;
import com.google.common.primitives.Longs;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Laura on 18/01/2018.
 */

public class EquipeChoiceDialog extends DialogFragment {

    // Use this instance of the interface to deliver action events
    NoticeDialogListener mListener;
    long itemId;
    Set item;

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Context contexte) {
        super.onAttach(contexte);
        // Verify that the host contexte implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (NoticeDialogListener) contexte;
        } catch (ClassCastException e) {
            // The contexte doesn't implement the interface, throw exception
            throw new ClassCastException(contexte.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public Dialog onCreateDialog(Bundle saveInstanceState){
        //recuperation des noms des equipes
        EquipeBDD equipeBDD = new EquipeBDD(getContext());
        equipeBDD.open();

        //recuperation de toutes les equipes
        Cursor cursor = equipeBDD.getAll();
        List<Integer> listnbSets = new ArrayList<>();
        List<String> equipesNoms = new ArrayList<>();
        List<Long> equipesId = new ArrayList<>();

        while(cursor.moveToNext()){
            int nbSets = equipeBDD.getNbSets(cursor.getLong(cursor.getColumnIndex(DatabaseHandler.ID)));
            if( nbSets < 6) {
                listnbSets.add(nbSets);
                equipesId.add(cursor.getLong(cursor.getColumnIndex(DatabaseHandler.ID))); //id de l'équipe
                equipesNoms.add(cursor.getString(cursor.getColumnIndex(DatabaseHandler.EQUIPE_NOM))); //nom de l'équipe
            }
        }
        cursor.close();
        equipeBDD.close();

        final long[]id = Longs.toArray(equipesId);
        final List<Integer> checkItems = listnbSets;
        item = getArguments().getParcelable("set");
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.choix_equipe)
                .setSingleChoiceItems(equipesNoms.toArray(new String[equipesNoms.size()]),0,new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which){
                        //on fait rien
                    }
                })
                .setPositiveButton(R.string.positive_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Send the positive button event back to the host activity
                        if(id.length != 0) {
                            ListView lw = ((AlertDialog) dialog).getListView();
                            itemId = id[lw.getCheckedItemPosition()];
                            String checkItem =  checkItems.get(lw.getCheckedItemPosition()).toString();
                            mListener.onDialogPositiveClick(EquipeChoiceDialog.this, checkItem,itemId,item);
                        }
                    }
                })
                .setNegativeButton(R.string.negative_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Send the negative button event back to the host activity
                        mListener.onDialogNegativeClick(EquipeChoiceDialog.this);
                    }
                });
        return builder.create();
    }
}

package com.example.lhure.pokebuilder.teambuilder.team;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.example.lhure.pokebuilder.R;
import com.example.lhure.pokebuilder.utils.NoticeDialogListener;

/**
 * Created by Laura on 25/01/2018.
 */

public class EquipeDeleteDialog extends DialogFragment {

    // Use this instance of the interface to deliver action events
    NoticeDialogListener mListener;

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Context contexte) {
        super.onAttach(contexte);
        // Verify that the host contexte implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (NoticeDialogListener) contexte;
        } catch (ClassCastException e) {
            // The contexte doesn't implement the interface, throw exception
            throw new ClassCastException(contexte.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public Dialog onCreateDialog(Bundle saveInstanceState) {
        //recuperation de l'id passe en parametre
        final long id= getArguments().getLong("equipeId");

        //creation de la pop-up de confirmation de la suppression de l'equipe
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.delete_confirmation)
                .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mListener.onDialogPositiveClick(EquipeDeleteDialog.this,"",id,null);
                    }
                })
                .setNegativeButton(R.string.negative_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mListener.onDialogNegativeClick(EquipeDeleteDialog.this);
                    }
                });
        return builder.create();
    }

}

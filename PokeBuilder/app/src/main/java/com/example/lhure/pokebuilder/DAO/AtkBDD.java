package com.example.lhure.pokebuilder.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.lhure.pokebuilder.Entity.Attaque;

/**
 * Created by space on 07/01/2018.
 */

public class AtkBDD extends DAOBase{

/////////////////////////// Constructeurs ///////////////////////
    public AtkBDD(Context ctxt){
        super(ctxt);
    }

    public long insertAtk(Attaque a){
        ContentValues values= new ContentValues();
        //Add values
        values.put(DatabaseHandler.ATK_NOM, a.getNom());
        Log.e("Async task debug","Début de l'insert en base");
        return db.insert(DatabaseHandler.ATK, null, values);
    }
    public long updateAtk(Attaque a){
        ContentValues values = new ContentValues();
        values.put(DatabaseHandler.ATK_NOM, a.getNom());
        return db.update(DatabaseHandler.ATK, values, DatabaseHandler.ID+" = "+a.getId(), null);
    }
    public long removeAtk(Attaque a){
        return db.delete(DatabaseHandler.ATK,DatabaseHandler.ID + " = "+a.getId(), null);
    }
}

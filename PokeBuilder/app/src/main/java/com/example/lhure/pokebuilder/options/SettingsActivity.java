package com.example.lhure.pokebuilder.options;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;

import com.example.lhure.pokebuilder.R;

/**
 * Created by Laura on 26/01/2018.
 */

public class SettingsActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        openPreferencesFragment(); //affichage des preferences

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(R.string.options); //changement du titre de l'action bar
    }

    /**
     * ouvre le fragment contenant les preferences
     */
    private void openPreferencesFragment() {
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content,new SettingsFragment())
                .commit();
    }
}

package com.example.lhure.pokebuilder.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.lhure.pokebuilder.Entity.Set;

/**
 * Created by space on 07/01/2018.
 */

public class SetsBDD extends DAOBase{

/////////////////////////// Constructeurs ///////////////////////
    public SetsBDD(Context ctxt){
        super(ctxt);
    }

    public long insertSet(Set s){
        ContentValues values= new ContentValues();
        //Add values
        values.put(DatabaseHandler.SETS_TIER, s.getTier());
        values.put(DatabaseHandler.SETS_NATURE, s.getNature());
        values.put(DatabaseHandler.SETS_TALENT, s.getTalent());
        values.put(DatabaseHandler.SETS_ATK1, s.getAttaques().get(0).getId());
        values.put(DatabaseHandler.SETS_ATK2, s.getAttaques().get(1).getId());
        values.put(DatabaseHandler.SETS_ATK3, s.getAttaques().get(2).getId());
        values.put(DatabaseHandler.SETS_ATK4, s.getAttaques().get(3).getId());
        values.put(DatabaseHandler.SETS_EXPORT, s.getShowDown());
        values.put(DatabaseHandler.SETS_PKMN, s.getPokemon().getId());
        values.put(DatabaseHandler.SETS_OBJET, s.getObjet().getId());
        values.put(DatabaseHandler.SETS_EV, s.getEV().getId());
        return db.insert(DatabaseHandler.SETS, null, values);
    }
    public long updatePkmn(Set s){
        ContentValues values = new ContentValues();
        //Add values
        values.put(DatabaseHandler.SETS_TIER, s.getTier());
        values.put(DatabaseHandler.SETS_NATURE, s.getNature());
        values.put(DatabaseHandler.SETS_TALENT, s.getTalent());
        values.put(DatabaseHandler.SETS_ATK1, s.getAttaques().get(0).getId());
        values.put(DatabaseHandler.SETS_ATK2, s.getAttaques().get(1).getId());
        values.put(DatabaseHandler.SETS_ATK3, s.getAttaques().get(2).getId());
        values.put(DatabaseHandler.SETS_ATK4, s.getAttaques().get(3).getId());
        values.put(DatabaseHandler.SETS_EXPORT, s.getShowDown());
        values.put(DatabaseHandler.SETS_PKMN, s.getPokemon().getId());
        values.put(DatabaseHandler.SETS_OBJET, s.getObjet().getId());
        values.put(DatabaseHandler.SETS_EV, s.getEV().getId());
        return db.update(DatabaseHandler.SETS, values, DatabaseHandler.ID+" = "+s.getId(), null);
    }
    public long removeSet(long id){
        return db.delete(DatabaseHandler.SETS,DatabaseHandler.ID + "=?",new String[]{String.valueOf(id)});
    }

    public Cursor getSetById(long id){
        return db.rawQuery("SELECT * FROM "+DatabaseHandler.SETS+" WHERE "+DatabaseHandler.ID+"=?",new String[]{String.valueOf(id)});
    }
}

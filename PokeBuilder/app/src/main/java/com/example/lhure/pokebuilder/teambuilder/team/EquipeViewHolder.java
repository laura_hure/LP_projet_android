package com.example.lhure.pokebuilder.teambuilder.team;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.example.lhure.pokebuilder.DAO.EquipeBDD;
import com.example.lhure.pokebuilder.DAO.SetsBDD;
import com.example.lhure.pokebuilder.Entity.Equipe;
import com.example.lhure.pokebuilder.Entity.Set;
import com.example.lhure.pokebuilder.PokeBuilderActivity;
import com.example.lhure.pokebuilder.R;

/**
 * Created by Laura on 23/01/2018.
 */

public class EquipeViewHolder extends RecyclerView.ViewHolder {

    private Context context;
    private TextView nom, tier;
    private Button supprimer;
    private ImageView show_partage;
    private LinearLayout listOfPokemons;
    private TextView[] pokemons;
    private Button[] pokemons_delete;

    /**
     * la view holder a besoin de la vue et du context
     * @param itemView
     * @param context
     */
    public EquipeViewHolder(View itemView, Context context) {
        super(itemView);
        this.context = context;

        nom = itemView.findViewById(R.id.equipe_nom);
        supprimer = itemView.findViewById(R.id.equipe_supprimer);
        show_partage = itemView.findViewById(R.id.show_arrow);

        //list des pokemon
        listOfPokemons = itemView.findViewById(R.id.listeOfPokemon);
        pokemons = new TextView[]{
                itemView.findViewById(R.id.pokemon1),
                itemView.findViewById(R.id.pokemon2),
                itemView.findViewById(R.id.pokemon3),
                itemView.findViewById(R.id.pokemon4),
                itemView.findViewById(R.id.pokemon5),
                itemView.findViewById(R.id.pokemon6)
        };

        //bouttons suppression des pokemon
        pokemons_delete = new Button[]{
                itemView.findViewById(R.id.pokemon1_supprimer),
                itemView.findViewById(R.id.pokemon2_supprimer),
                itemView.findViewById(R.id.pokemon3_supprimer),
                itemView.findViewById(R.id.pokemon4_supprimer),
                itemView.findViewById(R.id.pokemon5_supprimer),
                itemView.findViewById(R.id.pokemon6_supprimer)
        };

        tier = itemView.findViewById(R.id.tier);

    }

    /**
     * prend une equipe_card en parametre et dispose son contenu dans la card view
     * @param equipe
     */
    public void bind(final Equipe equipe){
        //nom de l'equipe
        nom.setText(equipe.getNom());
        if(equipe.getSets().size()>= 1) {
           tier.setText("(" +equipe.getSets().get(0).getTier() +")");
        }

        //affichage des pokemons de la card
        for(int i=0;i<equipe.getSets().size();i++){
            pokemons[i].setText(equipe.getSets().get(i).getShowDown()); //remplissage des pokemons
            pokemons_delete[i].setVisibility(View.VISIBLE); //boutton de suppression visible

            //suppression du pokemon au clic sur le bouton
            final int index = i;
            final TextView pokemon = pokemons[i];
            final Button button = pokemons_delete[i];
            pokemons_delete[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //on masque le text showdown et du button
                    pokemon.setText("");
                    button.setVisibility(View.GONE);

                    // suppression du set dans l'equipe
                    EquipeBDD equipeBDD = new EquipeBDD(context);
                    equipeBDD.open();
                    equipeBDD.removeSet(index,equipe.getId());
                    equipeBDD.close();

                    //suppression du pokemon en base
                    SetsBDD setsBDD = new SetsBDD(context);
                    setsBDD.open();
                    setsBDD.removeSet(equipe.getSets().get(index).getId());
                    setsBDD.close();
                }
            });
        }

        //listener pour montrer la liste des sets de l'equipe
        show_partage.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                if(listOfPokemons.getVisibility() == View.GONE){
                    show_partage.setImageResource(R.drawable.ic_show_arrow_top);
                    listOfPokemons.setVisibility(View.VISIBLE);
                }else{
                    show_partage.setImageResource(R.drawable.ic_show_arrow_bottom);
                    listOfPokemons.setVisibility(View.GONE);
                }
            }
        });

        //Lors de la suppression l'identifiant de l'equipe est passe a la pop-up de confirmation
        final long equipeId = equipe.getId();
        supprimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putLong("equipeId",equipeId);

                EquipeDeleteDialog equipeDeleteDialog = new EquipeDeleteDialog();
                equipeDeleteDialog.setArguments(bundle);

                equipeDeleteDialog.show(((PokeBuilderActivity)context).getSupportFragmentManager(),"supprimer_equipe");
            }
        });
    }

}

package com.example.lhure.pokebuilder.DAO;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Laura on 21/01/2018.
 */

public abstract class DAOBase {
    // Nous sommes à la première version de la base
    // Si je décide de la mettre à jour, il faudra changer cet attribut
    protected final static int VERSION = 1;

    private static final String NOM_BDD="pokebuilder.db";

    protected SQLiteDatabase db = null;
    protected DatabaseHandler dbHandler = null;

    public DAOBase(Context context) {
        this.dbHandler = new DatabaseHandler(context, NOM_BDD, null, VERSION);
    }

    public SQLiteDatabase open(){
        db = dbHandler.getWritableDatabase();
        return db;
    }

    public void close(){
        db.close();
    }

    public SQLiteDatabase getDb(){
        return db;
    }
}

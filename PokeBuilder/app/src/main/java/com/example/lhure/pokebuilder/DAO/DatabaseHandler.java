package com.example.lhure.pokebuilder.DAO;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by space on 07/01/2018.
 */

public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int VERSION_BDD=1;
    //ID qui sera nome pareil pour toute les tables
    public static final String ID = "_id";

    //Table Equipe
    public static final String EQUIPE = "EQUIPE";
    public static final String EQUIPE_NOM = "NOM";
    public static final String EQUIPE_SET1 = "SET1";
    public static final String EQUIPE_SET2 = "SET2";
    public static final String EQUIPE_SET3 = "SET3";
    public static final String EQUIPE_SET4 = "SET4";
    public static final String EQUIPE_SET5 = "SET5";
    public static final String EQUIPE_SET6 = "SET6";

    //Table Item
    public static final String ITEM = "ITEM";
    public static final String ITEM_NOM = "NOM";
    public static final String ITEM_DESCRIPTION = "DESCRIPTION";

    //Table IV
    private static final String IV = "IV";
    private static final String IV_HP = "HP";
    private static final String IV_ATK = "ATK";
    private static final String IV_DEF = "DEF";
    private static final String IV_ATKSPE = "ATKSPE";
    private static final String IV_DEFSPE = "DEFSPE";
    private static final String IV_VIT = "VIT";

    //Table EV
    public  static final String EV = "EV";
    public static final String EV_HP = "HP";
    public static final String EV_ATK = "ATK";
    public static final String EV_DEF = "DEF";
    public static final String EV_ATKSPE = "ATKSPE";
    public static final String EV_DEFSPE = "DEFSPE";
    public static final String EV_VIT = "VIT";

    //Table Pkmn
    public static final String PKMN = "PKMN";
    public static final String PKMN_NOM = "NOM";
    public static final String PKMN_IMG = "IMG";

    //Table ATK
    public static final String ATK = "ATK";
    public static final String ATK_NOM = "NOM";

    //Table Set
    public static final String SETS = "SETS";
    public static final String SETS_TIER = "TIER";
    public static final String SETS_NATURE = "NATURE";
    public static final String SETS_TALENT = "TALENT";
    public static final String SETS_ATK1 = "ATK1";
    public static final String SETS_ATK2 = "ATK2";
    public static final String SETS_ATK3 = "ATK3";
    public static final String SETS_ATK4 = "ATK4";
    public static final String SETS_PKMN = "PKMN";
    public static final String SETS_OBJET = "OBJET";
    public static final String SETS_IV = "IV";
    public static final String SETS_EV = "EV";
    public static final String SETS_EXPORT = "EXPORT";


    //Script de créationl:

    //----------------------------TABLE ITEM----------------------------------
    private static final String CREATE_ITEM_TABLE = "CREATE TABLE " + ITEM + " ("
            + ID + " INTEGER PRIMARY KEY, "
            + ITEM_NOM + " TEXT NOT NULL, "
            + ITEM_DESCRIPTION + " TEXT);";

    private static final String DROP_ITEM_TABLE = "DROP TABLE IF EXISTS " + ITEM + ";";

    // + "CREATE TABLE "+IV+" ("+IV_ID+" PRIMARY KEY NOT NULL AUTOINCREMENT, "+IV_HP+", "+IV_ATK+", "+IV_DEF+", "+IV_ATKSPE+", "+IV_DEFSPE+", "+IV_VIT+");"

    //----------------------------TABLE EV------------------------------------
    private static final String CREATE_EV_TABLE = "CREATE TABLE " + EV + " ("
            + ID + " INTEGER PRIMARY KEY, "
            + EV_HP + " INTEGER, "
            + EV_ATK + " INTEGER, "
            + EV_DEF + " INTEGER, "
            + EV_ATKSPE + " INTEGER, "
            + EV_DEFSPE + " INTEGER, "
            + EV_VIT + " INTEGER);";

    private static final String DROP_EV_TABLE = "DROP TABLE IF EXISTS " + EV + ";";

    //-----------------------TABLE POKEMON---------------------------------------
    private static final String CREATE_PKMN_TABLE = "CREATE TABLE " + PKMN + " ("
            + ID + " INTEGER PRIMARY KEY, "
            + PKMN_NOM + " TEXT NOT NULL, "
            + PKMN_IMG + " TEXT);";

    private static final String DROP_PKMN_TABLE = "DROP TABLE IF EXISTS " + PKMN + ";";

    //----------------------------------TABLE ATTAQUE--------------------------------
    private static final String CREATE_ATK_TABLE = "CREATE TABLE " + ATK + " ("
            + ID + " INTEGER PRIMARY KEY, "
            + ATK_NOM + " TEXT NOT NULL);";

    private static final String DROP_ATK_TABLE = "DROP TABLE IF EXISTS " + ATK + ";";

    //---------------------------TABLE SETS---------------------------------------
    private static final String CREATE_SET_TABLE = "CREATE TABLE " + SETS + " ("
            + ID + " INTEGER PRIMARY KEY, "
            + SETS_TIER + " TEXT, "
            + SETS_NATURE + " TEXT, "
            + SETS_TALENT + " TEXT, "
            + SETS_ATK1 + " INTEGER, "
            + SETS_ATK2 + " INTEGER, "
            + SETS_ATK3 + " INTEGER, "
            + SETS_ATK4 + " INTEGER, "
            + SETS_PKMN + " INTEGER, "
            + SETS_OBJET + " INTEGER, "
            //+ SETS_IV + " INTEGER, "
            + SETS_EV + " INTEGER, "
            + SETS_EXPORT + " TEXT,"
            + " FOREIGN KEY(" + SETS_PKMN + ") REFERENCES " + PKMN + "(" + ID + "), "
            + " FOREIGN KEY(" + SETS_OBJET + ") REFERENCES " + ITEM + "(" + ID + "), "
            + " FOREIGN KEY(" + SETS_ATK1 + ") REFERENCES " + ATK + "(" + ID + "),"
            + " FOREIGN KEY(" + SETS_ATK2 + ") REFERENCES " + ATK + "(" + ID + "),"
            + " FOREIGN KEY(" + SETS_ATK3 + ") REFERENCES " + ATK + "(" + ID + "),"
            + " FOREIGN KEY(" + SETS_ATK4 + ") REFERENCES " + ATK + "(" + ID + "));";

    private static final String DROP_SET_TABLE = "DROP TABLE IF EXISTS " + SETS + ";";

    //--------------------------TABLE EQUIPE---------------------------------
    private static final String CREATE_EQUIPE_TABLE = "CREATE TABLE " + EQUIPE + " ("
            + ID + " INTEGER PRIMARY KEY, "
            + EQUIPE_NOM + " TEXT NOT NULL, "
            + EQUIPE_SET1 + " INTEGER, "
            + EQUIPE_SET2 + " INTEGER, "
            + EQUIPE_SET3 + " INTEGER, "
            + EQUIPE_SET4 + " INTEGER, "
            + EQUIPE_SET5 + " INTEGER, "
            + EQUIPE_SET6 + " INTEGER, "
            + " FOREIGN KEY (" + EQUIPE_SET1 + ") REFERENCES " + SETS + "(" + ID + "),"
            + " FOREIGN KEY (" + EQUIPE_SET2 + ") REFERENCES " + SETS + "(" + ID + "),"
            + " FOREIGN KEY (" + EQUIPE_SET3 + ") REFERENCES " + SETS + "(" + ID + "),"
            + " FOREIGN KEY (" + EQUIPE_SET4 + ") REFERENCES " + SETS + "(" + ID + "),"
            + " FOREIGN KEY (" + EQUIPE_SET5 + ") REFERENCES " + SETS + "(" + ID + "),"
            + " FOREIGN KEY (" + EQUIPE_SET6 + ") REFERENCES " + SETS + "(" + ID + ")"
            + ");";

    private static final String DROP_EQUIPE_TABLE = "DROP TABLE IF EXISTS " + EQUIPE + ";";

    ///////////////////////// Constructeur + onFunctions ///////////////////////////////////////////////
    public DatabaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) { // + (, DatabaseErrorHandler errorHandler)
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_ITEM_TABLE);
        db.execSQL(CREATE_EV_TABLE);
        db.execSQL(CREATE_ATK_TABLE);
        db.execSQL(CREATE_PKMN_TABLE);
        db.execSQL(CREATE_SET_TABLE);
        db.execSQL(CREATE_EQUIPE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_EQUIPE_TABLE);
        db.execSQL(DROP_SET_TABLE);
        db.execSQL(DROP_PKMN_TABLE);
        db.execSQL(DROP_ATK_TABLE);
        db.execSQL(DROP_EV_TABLE);
        db.execSQL(DROP_ITEM_TABLE);
        onCreate(db);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {//Optionnel
    }

}

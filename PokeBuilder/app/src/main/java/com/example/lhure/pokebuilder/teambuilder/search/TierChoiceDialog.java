package com.example.lhure.pokebuilder.teambuilder.search;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.widget.ListView;

import com.example.lhure.pokebuilder.R;
import com.example.lhure.pokebuilder.utils.NoticeDialogListener;

/**
 * Created by Laura on 14/01/2018.
 */

public class TierChoiceDialog extends DialogFragment {

    //item selectionn
    private String checkedItem;


    // Use this instance of the interface to deliver action events
    NoticeDialogListener mListener;

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Context contexte) {
        super.onAttach(contexte);
        // Verify that the host contexte implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (NoticeDialogListener) contexte;
        } catch (ClassCastException e) {
            // The contexte doesn't implement the interface, throw exception
            throw new ClassCastException(contexte.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle saveInstanceState){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.choix_tier)
                .setSingleChoiceItems(R.array.tiers,0,new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which){
                        //on fait rien
                    }
                })
                .setPositiveButton(R.string.positive_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Send the positive button event back to the host activity
                        ListView lw = ((AlertDialog)dialog).getListView();
                        checkedItem = (String)lw.getAdapter().getItem(lw.getCheckedItemPosition());
                        mListener.onDialogPositiveClick(TierChoiceDialog.this,checkedItem,0,null);
                    }
                })
                .setNegativeButton(R.string.negative_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Send the negative button event back to the host activity
                        mListener.onDialogNegativeClick(TierChoiceDialog.this);
                    }
                });
        return builder.create();
    }
}

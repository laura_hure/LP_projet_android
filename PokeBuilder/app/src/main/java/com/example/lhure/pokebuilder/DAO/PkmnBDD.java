package com.example.lhure.pokebuilder.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.lhure.pokebuilder.Entity.Pokemon;

/**
 * Created by space on 07/01/2018.
 */

public class PkmnBDD extends DAOBase{

/////////////////////////// Constructeurs ///////////////////////
    public PkmnBDD(Context ctxt){
        super(ctxt);
    }

    public long insertPkmn(Pokemon p){
        ContentValues values= new ContentValues();
        //Add values
        values.put(DatabaseHandler.PKMN_NOM, p.getText());
        values.put(DatabaseHandler.PKMN_IMG,p.getImage());
        Log.e("Async task debug","Début de l'insert en base");
        return db.insert(DatabaseHandler.PKMN, null, values);
    }
    public long updatePkmn(Pokemon p){
        ContentValues values = new ContentValues();
        //Add Values
        values.put(DatabaseHandler.PKMN_NOM, p.getText());
        values.put(DatabaseHandler.PKMN_IMG,p.getImage());
        return db.update(DatabaseHandler.PKMN, values, DatabaseHandler.ID+" = "+p.getId(), null);
    }
    public long removePkmn(Integer id){
        return db.delete(DatabaseHandler.PKMN,DatabaseHandler.ID + " = "+id, null);
    }
}

package com.example.lhure.pokebuilder.DAO;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by space on 07/01/2018.
 */

public class IVBDD {
    private static final int VERSION_BDD=1;                 //Version de la base
    private static final String NOM_BDD="pokebuilder.db";   //nom de la base ou trouver la table
    private static final String IV="IV";

    private static final String COL_ID="ID";

    private static final String COL_HP="HP";

    private static final String COL_ATK="ATK";

    private static final String COL_DEF="DEF";

    private static final String COL_ATKSPE="ATKSPE";

    private static final String COL_DEFSPE="DEFSPE";

    private static final String COL_VIT="VIT";

    private SQLiteDatabase bdd;
    private DatabaseHandler base;

/////////////////////////// Constructeurs ///////////////////////
    public IVBDD(Context ctxt){
        base=new DatabaseHandler(ctxt,NOM_BDD, null,VERSION_BDD);
    }

////////////////////////////// Utils ////////////////////////////
    public void open(){
        bdd = base.getWritableDatabase();
    }
    public void close(){
        bdd.close();
    }

    /*public long insertIv(Iv i){
        ContentValues values= new ContentValues();
        //Add values
        //values.put(COL_ID,i.getId());
        values.put(COL_HP, i.getPv);
        values.put(COL_ATK, i.getAtk());
        values.put(COL_DEF, i.getDef());
        values.put(COL_ATKSPE, i.getAtkSpe());
        values.put(COL_DEFSPE, i.getDefSpe());
        values.put(COL_VIT, i.getVit());
        Log.e("Async task debug","Début de l'insert en base");
        return bdd.insert(IV, null, values);
    }
    public long updateEquipe(Integer id, Iv i){
        ContentValues values = new ContentValues();
        //Add Values
        //values.put(COL_ID,i.getId());
        values.put(COL_HP, i.getHP());
        values.put(COL_ATK, i.getAtk());
        values.put(COL_DEF, i.getDef());
        values.put(COL_ATKSPE, i.getAtkSpe());
        values.put(COL_DEFSPE, i.getDefSpe());
        values.put(COL_VIT, i.getVit());
        return bdd.update(IV, values, COL_ID+" = "+id, null);
    }
    public long removeItem(Integer i){
        return bdd.delete(IV,COL_ID + " = "+id, null);
    }*/
}

package com.example.lhure.pokebuilder.Entity;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Laura on 16/01/2018.
 */

public class Equipe implements Parcelable{

    private int id;
    private String nom;
    private List<Set> sets = new ArrayList<>();

    public Equipe(String nom){
        this.nom = nom;
    }

    public Equipe(int id, String nom){
        this.id = id;
        this.nom = nom;
    }

    public String getNom(){return this.nom;}
    public int getId(){return this.id;}
    public List<Set> getSets(){return sets;}

    /* Factory Creator */
    public static final Creator<Equipe> CREATOR = new Creator<Equipe>() {
        @Override
        public Equipe createFromParcel(Parcel in) {
            return new Equipe(in);
        }

        @Override
        public Equipe[] newArray(int size) {
            return new Equipe[size];
        }
    };

    private Equipe(Parcel in){
        this.id = in.readInt();

        this.nom = in.readString();

        this.sets = new ArrayList<>();
        in.readList(sets,Set.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(nom);
        dest.writeList(sets);

    }
}

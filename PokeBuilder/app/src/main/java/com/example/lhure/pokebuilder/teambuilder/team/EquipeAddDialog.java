package com.example.lhure.pokebuilder.teambuilder.team;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.lhure.pokebuilder.Entity.Equipe;
import com.example.lhure.pokebuilder.R;
import com.example.lhure.pokebuilder.DAO.EquipeBDD;
import com.example.lhure.pokebuilder.utils.NoticeDialogListener;

/**
 * Created by Laura on 20/01/2018.
 */

public class EquipeAddDialog extends DialogFragment {

    // Use this instance of the interface to deliver action events
    NoticeDialogListener mListener;
    String nom; //nom de la nouvelle equipe_card

    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
    @Override
    public void onAttach(Context contexte) {
        super.onAttach(contexte);
        // Verify that the host contexte implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (NoticeDialogListener) contexte;
        } catch (ClassCastException e) {
            // The contexte doesn't implement the interface, throw exception
            throw new ClassCastException(contexte.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public Dialog onCreateDialog(Bundle saveInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.ajouter_equipe,null);

        builder.setTitle(R.string.ajouter_equipe)
                .setView(view)
                .setPositiveButton(R.string.creer, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //recuperaion du nom de l'equipe_card
                        EditText edit = view.findViewById(R.id.nom_new_equipe);
                        nom = edit.getText().toString();

                        //ajout de l'équipe
                        new AjouterEquipe().execute(nom);

                        mListener.onDialogPositiveClick(EquipeAddDialog.this,nom,0,null);
                    }
                })
                .setNegativeButton(R.string.negative_button, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        return builder.create();
    }

    private class AjouterEquipe extends AsyncTask<String,Integer,Long> {

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected Long doInBackground(String... noms) {
            //creation et ouverture de la base de données
            EquipeBDD equipeBDD = new EquipeBDD(getActivity());
            equipeBDD.open();
            Long insert = new Long(-1);

            for (String nom : noms) {
                String name = nom.replace("\n"," ").trim();

                if(name.length() > 20)
                    return (long)-2;
                else if(name.length() == 0)
                    return (long)-3;
                else
                    insert = equipeBDD.insertEquipe(new Equipe(name));

                equipeBDD.close();
                return insert;
            }
            return insert;
        }

        @Override
        protected void onPostExecute(Long result) {
            if(result == -1)
                Toast.makeText(getContext(), "erreur lors de la création (nom trop long ?)",Toast.LENGTH_SHORT ).show();
            else if(result == -2)
                Toast.makeText(getContext(),"nom trop long",Toast.LENGTH_SHORT).show();
            else if(result == -3)
                Toast.makeText(getContext(),"l'équipe doit avoir un nom",Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(getContext(), "Ajout réussi",Toast.LENGTH_SHORT ).show();
        }
    }
}

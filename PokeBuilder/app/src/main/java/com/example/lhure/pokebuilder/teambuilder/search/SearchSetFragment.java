package com.example.lhure.pokebuilder.teambuilder.search;


import android.app.Fragment;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.lhure.pokebuilder.Entity.Attaque;
import com.example.lhure.pokebuilder.Entity.Ev;
import com.example.lhure.pokebuilder.Entity.Objet;
import com.example.lhure.pokebuilder.Entity.Pokemon;
import com.example.lhure.pokebuilder.Entity.Set;
import com.example.lhure.pokebuilder.PokeBuilderActivity;
import com.example.lhure.pokebuilder.R;
import com.example.lhure.pokebuilder.utils.NetworkChangeReceiver;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Laura on 07/01/2018.
 */

public class SearchSetFragment extends Fragment {


    protected RecyclerView mRecyclerView;
    protected SearchSetAdapter mAdapter;
    protected RecyclerView.LayoutManager mLayoutManager;
    private ProgressBar progressBar;
    public List<Set> sets = new ArrayList<>();

    @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_recyclerview_search_set, container, false);

        mRecyclerView = rootView.findViewById(R.id.recyclerView);
        progressBar = rootView.findViewById(R.id.progress_bar);

        // LinearLayoutManager is used here, this will layout the elements in a similar fashion
        // to the way ListView would layout elements. The RecyclerView.LayoutManager defines how
        // elements are laid out.
        mLayoutManager = new LinearLayoutManager(getActivity());

        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new SearchSetAdapter(getActivity(), sets);
        mRecyclerView.setAdapter(mAdapter);

        //recuperatiion des donnees passe en arguments
        Bundle bundle = getArguments();
        if(bundle != null){
            String args = bundle.getString("tier");
            new DownloadSet().execute(args);
        }else{
            new DownloadSet().execute();
        }

        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }


    private class DownloadSet extends AsyncTask<String, Integer, List> {

        private final String LOG_TAG = "SearchSetFragment";

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(List result) {

            progressBar.setVisibility(View.GONE);
            //on signale la modification de données
            mAdapter.notifyDataSetChanged();
        }

        @Override
        protected List doInBackground(String... params) {
            getSetPokemon(params);
            return null;
        }

        private void getSetPokemon(String... params) {

            //vidage de la liste
            mAdapter.clear();

            //-------------------gestion des parametres de recherche-------------
            String url = "http://pokestrat.com/set-strategique/resultat.php?";
            String tier = "";
            for (String param : params) {
                    tier = param;
                    int id = getContext().getResources().getIdentifier(param.replace(" ",""),"integer",getContext().getPackageName());
                    url += "tier=" + getContext().getResources().getInteger(id);
            }

            //----------------------recuperation des set-------------
            Document page = null;
            try {
                //recuperation de la page avec le tier en filtre
                page = Jsoup.connect(url).get();

                //on recupere tous les set
                Elements base = page.getElementsByClass("base");
                String imageURL = "";
                int cpt = 0;
                for (Element set : base) {

                    //on ne prend pas en compte le premier element trouvé qui n'est pas un set
                    if (set != base.first()) {
                        cpt++; //compteur pour savoir dans quel set on est

                        //recuperation du titre
                        Element titre = set.getElementsByClass("titre_set").first();

                        //on recupere le nom du pokemon
                        String nom = titre.getElementsByTag("a").first().text();
                        Log.d(LOG_TAG,"nom : " + nom);

                        //nom anglais du pokemon
                        String nomEN = getNomPokemonEn(titre);
                        Log.d(LOG_TAG, "nom en : " + nomEN);

                        //recuperation de l'URL de l'image du pokemon
                        imageURL = "https://img.pokemondb.net/artwork/" + nomEN.toLowerCase() + ".jpg";
                        Log.d(LOG_TAG, "image url : " + imageURL);

                        //creation du pokemon
                        Pokemon pokemon = new Pokemon(nom, imageURL, nomEN);

                        //recuperation de l'objet
                        String objetNom = set.getElementsByClass("details").first().getElementsByTag("a").first().text();
                        Log.d(LOG_TAG,"nom objet : " + objetNom);

                        //creation de l'objet
                        Objet objet = new Objet(objetNom);

                        //recuperation du talent du pokemon
                        String talent = set.getElementsByClass("details").first().getElementsByTag("a").get(1).text();
                        Log.d(LOG_TAG, "talent : " + talent);

                        //recuperation de la nature du pokemon
                        String nature = set.getElementsByClass("details").first().ownText();
                        Log.d(LOG_TAG, "nature : " + nature);

                        //creation de l'objet EV
                        Ev ev = getEV(set);

                        //recuperation du contenu show down
                        String showDown = set.getElementById("temptext" + cpt).ownText();
                        Log.d(LOG_TAG, "showdown : " + showDown);

                        //creation du set
                        Set setPokemon = new Set(pokemon,objet,ev,talent, nature, showDown);
                        setPokemon.setTier(tier); //on attribue le tier au set

                        //met les attaques dans le pokemon
                        getAttaques(set,setPokemon);

                        //Ajout du pokemon a l'adapter
                        mAdapter.add(setPokemon);

                    }
                }

            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }

        /**
         * à partir du titre trouve le nom en du pokemon
         * @param titre
         * @return nom en du pokemon
         */
        private String getNomPokemonEn(Element titre) {
            try {
                //on recupère le nom EN du pokemon
                String[] words = titre.getElementsByTag("span").get(2).text().split("\\(");
                //nom anglais du pokemon
                String nomEN = "";
                if (words[1].compareTo("Méga) ") == 0 || words[1].compareTo("Noyau) ") == 0 || words[1].compareTo("Primo) ") == 0)
                    return words[2].substring(0, words[2].length() - 1);
                else
                    return words[1].substring(0, words[1].length() - 1);
            }catch (Exception e) {
                return "";
            }
        }

        /**
         * recupere les ev
         * @param set
         * @return un objet Ev
         */
        private Ev getEV(Element set){
            try {
                //recuperation de EV
                Elements EV = set.getElementsByClass("set_strat_ev").first().children();

                String pv = EV.get(1).text();
                Log.d(LOG_TAG,"pv : " + pv);

                String att = EV.get(5).text();
                Log.d(LOG_TAG,"att : " + att );

                String def = EV.get(9).text();
                Log.d(LOG_TAG,"def : " + def);

                String Satt = EV.get(13).text();
                Log.d(LOG_TAG, "satt : " + Satt);

                String Sdef = EV.get(17).text();
                Log.d(LOG_TAG,"sdef : " + Sdef);

                String vit = EV.get(21).text();
                Log.d(LOG_TAG,"vit : " + vit);

                //creation de l'objet EV
                return new Ev(pv,att,def,Satt,Sdef,vit);
            }catch (Exception e) {
                return null;
            }
        }

        /**
         * ajoutes les attatques a la liste des attaques du set
         * @param set
         * @param setPokemon
         */
        private void getAttaques(Element set,Set setPokemon){
            try {
                //liste des attaques
                Elements attaques = set.getElementsByClass("attaques").first().children();
                //Ajout des attaques au pokemon
                setPokemon.getAttaques().add(new Attaque(attaques.get(2).text()));
                Log.d(LOG_TAG,"attaque 1 : " + attaques.get(2).text());

                setPokemon.getAttaques().add(new Attaque(attaques.get(6).text()));
                Log.d(LOG_TAG, "attaque 2 : " + attaques.get(6).text());

                setPokemon.getAttaques().add(new Attaque(attaques.get(10).text()));
                Log.d(LOG_TAG, "attaque 3 : " + attaques.get(10).text());

                setPokemon.getAttaques().add(new Attaque(attaques.get(14).text()));
                Log.d(LOG_TAG,"attaque 4 : " + attaques.get(14).text());

            }catch (Exception e){
                Log.e(LOG_TAG,e.getMessage());
            }
        }
    }

}

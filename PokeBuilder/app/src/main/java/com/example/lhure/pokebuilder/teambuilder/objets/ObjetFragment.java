package com.example.lhure.pokebuilder.teambuilder.objets;

import android.app.Fragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.example.lhure.pokebuilder.Entity.Objet;
import com.example.lhure.pokebuilder.R;

import java.util.ArrayList;
import java.util.List;

import me.sargunvohra.lib.pokekotlin.client.PokeApi;
import me.sargunvohra.lib.pokekotlin.client.PokeApiClient;
import me.sargunvohra.lib.pokekotlin.model.NamedApiResource;

/**
 * Created by Nicolas on 16/01/2018.
 */

public class ObjetFragment extends Fragment {

    private final String LOG_TAG = "ItemFragment";

    private ListView listItem;
    public ProgressBar progressBar;
    public Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.objets_list, container, false);
        listItem = view.findViewById(R.id.list_objets);
        progressBar = view.findViewById(R.id.progress_bar);


        //si une requete a ete passe on le passe au download
        if (getArguments() != null && !getArguments().getString("query").isEmpty())
            new DownloadItem().execute(getArguments().getString("query"));
        else
            new DownloadItem().execute();
        return view;
    }


    private class DownloadItem extends AsyncTask<String, Integer, List<Objet>> {

        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected List<Objet> doInBackground(String... queries) {
            PokeApi pokeApi = new PokeApiClient();
            List<Objet> objets = new ArrayList<Objet>();
            try {
                List<NamedApiResource> itemList = pokeApi.getItemList(0, 1000).getResults();
                List<Objet> names = new ArrayList<>();

                for (int i = 0; i < itemList.size(); i++) {
                    if (queries.length == 0)
                        names.add(new Objet(itemList.get(i).getName()));
                    else {
                        if (queries[0].compareTo(itemList.get(i).getName()) == 0) {
                            names.add(new Objet(itemList.get(i).getName()));
                        } else {
                            //on parcours les characters du dernier au premier
                            for (int end = queries[0].length(); end > 0; end--) {
                                //si on la sous-chaine est identique on l'ajoute aux résultat
                                if (itemList.get(i).getName().trim().length() >= end && queries[0].substring(0, end).compareTo(itemList.get(i).getName().substring(0, end)) == 0) {
                                    names.add(new Objet(itemList.get(i).getName()));
                                    break;
                                }
                            }
                        }
                    }
                    //Arrays.sort(names);
                }
                return names;
            } catch (Exception e) {
                Log.e("AsyncTask error", e.getMessage());
            }
            return new ArrayList<Objet>();
        }

        @Override
        protected void onPostExecute(List<Objet> result) {

            progressBar.setVisibility(View.GONE);

            if(result != null && result.size()!=0) {
                ObjetAdapter objAdapt = new ObjetAdapter(getContext(), R.layout.objets, result);
                listItem.setAdapter(objAdapt);
            }
        }
    }
}

package com.example.lhure.pokebuilder.teambuilder.objets;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.lhure.pokebuilder.Entity.Objet;
import com.example.lhure.pokebuilder.R;

import java.util.List;

/**
 * Created by nfalguieres on 25/01/18.
 */

public class ObjetAdapter extends ArrayAdapter {

    public ObjetAdapter(@NonNull Context context, int resource, @NonNull List<Objet> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup container) {
        //si la convertView n'a pas encore été crée on la crée
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.objets, container, false);
        }
        ((TextView)convertView.findViewById(R.id.nom_objet)).setText(((Objet)getItem(position)).getName());
        ((TextView)convertView.findViewById(R.id.description_objet)).setText(((Objet)getItem(position)).getDescription());

        return convertView;
    }
}

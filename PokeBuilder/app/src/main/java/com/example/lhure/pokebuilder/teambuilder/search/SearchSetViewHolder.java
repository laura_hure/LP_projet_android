package com.example.lhure.pokebuilder.teambuilder.search;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.lhure.pokebuilder.Entity.Set;
import com.example.lhure.pokebuilder.PokeBuilderActivity;
import com.example.lhure.pokebuilder.R;
import com.example.lhure.pokebuilder.teambuilder.team.EquipeChoiceDialog;


/**
 * Created by Laura on 07/01/2018.
 */

public class SearchSetViewHolder extends RecyclerView.ViewHolder {


    //----------------CHAMPS PRIVES--------------------------------
    private Context context;
    private TextView textView, attaque1, attaque2, attaque3, attaque4, objet_nom, talent, nature,
            pv, att, def, satt, sdef, vit, showDown;
    private ImageView imageView, arrow_attaques, arrow_details, arrow_ev, arrow_partage;
    private View attaques, list_attaques, details, list_details, ev, list_ev, partage, list_partage;
    private Button add;

    //itemView est la vue correspondante à 1 cellule
    public SearchSetViewHolder(View itemView) {
        super(itemView);

        //c'est ici que l'on fait nos findView

        initPokemonView(itemView);//le pokemon
        initAttaquesView(itemView);  //les attaques du pokemon
        initDetailsView(itemView); //les details du pokemon
        initStatistiquesView(itemView); //statistiques (ev)
        initPartageView(itemView); //contenu showdown pour le partage
    }

    public void setContext(Context context){
        this.context = context;
    }

    /**
     * Rempli la card en fonction d'un set de pokemon
     * @param set
     */
    public void bind(Set set){
        initPokemonView(set); //image et noms du pokemon
        initAttaquesView(set); //liste des attaques
        initDetailsView(set); //liste des details
        initStatistiquesView(set); //statistique (ev)
        initPartageView(set); //inititalisation du partage

    }

    /**
     * recupere les view pour l'image et le nom fr et en du pokemon dans la card
     * @param itemView
     */
    private void initPokemonView(View itemView){
        textView = itemView.findViewById(R.id.text);
        imageView =  itemView.findViewById(R.id.pokemon_image);
        add = itemView.findViewById(R.id.add_set);
    }

    /**
     * Initialise la cardView avec l'image et les noms fr et en du pokemon
     * @param set
     */
    private void initPokemonView(final Set set){
        textView.setText(set.getPokemon().getText() + " (" +set.getPokemon().getNameEN() + ")");
        Glide.with(context).load(set.getPokemon().getImage()).into( imageView);

        //ajout d'un listener pour ajouter le pokemon a l'equipe_card
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //lance la fenetre qui va demander dans quel equipe on ajoute le set selectionne
                Bundle bundle = new Bundle();
                bundle.putParcelable("set",set);
                DialogFragment newFragment = new EquipeChoiceDialog();
                newFragment.setArguments(bundle);
                newFragment.show(((PokeBuilderActivity)context).getSupportFragmentManager(),"choix_equipes");
            }
        });
    }

    /**
     * recupere les view pour l'affichage des attaques dans la card
     * @param itemView
     */
    private void initAttaquesView(View itemView){
        attaques = itemView.findViewById(R.id.show_attaques);
        list_attaques = itemView.findViewById(R.id.list_attaque);
        attaque1 = itemView.findViewById(R.id.attaque1);
        attaque2 = itemView.findViewById(R.id.attaque2);
        attaque3 = itemView.findViewById(R.id.attaque3);
        attaque4 = itemView.findViewById(R.id.attaque4);
        arrow_attaques = itemView.findViewById(R.id.arrow_attaques);
    }

    /**
     * initialise les attaques de la card et implement un listener pour le bouton qui hide/show les attaques
     * @param set
     */
    private void initAttaquesView(Set set){
        attaque1.setText(set.getAttaques().get(0).getNom());
        attaque2.setText(set.getAttaques().get(1).getNom());
        attaque3.setText(set.getAttaques().get(2).getNom());
        attaque4.setText(set.getAttaques().get(3).getNom());

        //Ajout d'un listener au button pour montrer les attaques
        attaques.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //si la liste des attaques n'est pas visible on la rend visible
                if(list_attaques.getVisibility() == View.GONE) {
                    list_attaques.setVisibility(View.VISIBLE);
                    arrow_attaques.setImageResource(R.drawable.ic_show_arrow_bottom);
                }
                else {
                    list_attaques.setVisibility(View.GONE);
                    arrow_attaques.setImageResource(R.drawable.ic_show_arrow);
                }
            }
        });
    }

    /**
     * recupere les views pour afficher les details dans la card
     * @param itemView
     */
    private void initDetailsView(View itemView){
        details = itemView.findViewById(R.id.show_details);
        list_details = itemView.findViewById(R.id.list_details);
        objet_nom = itemView.findViewById(R.id.objet_nom);
        talent = itemView.findViewById(R.id.talent);
        nature = itemView.findViewById(R.id.nature);
        arrow_details = itemView.findViewById(R.id.arrow_details);
    }

    /**
     * initialise les details de la card et implemente le listener qui hide/show les details
     * @param set
     */
    private void initDetailsView(Set set){
        objet_nom.setText(context.getString(R.string.objet) + " " + set.getObjet().getName());
        talent.setText(context.getString(R.string.talent) + " " + set.getTalent());
        nature.setText(context.getString(R.string.nature) + " " + set.getNature());

        //Ajout d'un listener au bouton pour montrer les details
        details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(list_details.getVisibility() == View.GONE){
                    list_details.setVisibility(View.VISIBLE);
                    arrow_details.setImageResource(R.drawable.ic_show_arrow_bottom);
                }else {
                    list_details.setVisibility(View.GONE);
                    arrow_details.setImageResource(R.drawable.ic_show_arrow);
                }
            }
        });
    }

    /**
     * recupere les view pour afficher les statistiques dans la card
     * @param itemView
     */
    private void initStatistiquesView(View itemView){
        ev = itemView.findViewById(R.id.show_ev);
        list_ev = itemView.findViewById(R.id.list_ev);
        pv = itemView.findViewById(R.id.pv);
        att = itemView.findViewById(R.id.att);
        def = itemView.findViewById(R.id.def);
        satt = itemView.findViewById(R.id.satt);
        sdef = itemView.findViewById(R.id.sdef);
        vit = itemView.findViewById(R.id.vit);
        arrow_ev = itemView.findViewById(R.id.arrow_ev);
    }

    /**
     * initialise les statistiques de la card et implement le listener qui hide/show les statistiques
     * @param set
     */
    private void initStatistiquesView(Set set){
        pv.setText(context.getString(R.string.pv) + " " + set.getEV().getPv());
        att.setText(context.getString(R.string.att) + " " + set.getEV().getAtt());
        def.setText(context.getString(R.string.def) + " " + set.getEV().getDef());
        satt.setText(context.getString(R.string.satt) + " " + set.getEV().getSAtt());
        sdef.setText(context.getString(R.string.sdef) + " " + set.getEV().getSDef());
        vit.setText(context.getString(R.string.vit) + " " + set.getEV().getVit());

        //Ajout d'un listener au bouton pour montrer les details
        ev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(list_ev.getVisibility() == View.GONE){
                    list_ev.setVisibility(View.VISIBLE);
                    arrow_ev.setImageResource(R.drawable.ic_show_arrow_bottom);
                }else {
                    list_ev.setVisibility(View.GONE);
                    arrow_ev.setImageResource(R.drawable.ic_show_arrow);
                }
            }
        });
    }

    /**
     * recupere les view pour l'affichage du partage dans la card
     * @param itemView
     */
    private void initPartageView(View itemView){
        partage = itemView.findViewById(R.id.show_partage);
        list_partage = itemView.findViewById(R.id.list_partage);
        showDown = itemView.findViewById(R.id.partage);
        arrow_partage = itemView.findViewById(R.id.arrow_partage);
    }

    /**
     * initialise le contenu du partage de la card et implement le listener qui hide/show le partage
     * @param set
     */
    private void initPartageView(Set set){
        showDown.setText(set.getShowDown());

        //Ajout du listener pour le bouton du partage showDown
        partage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(list_partage.getVisibility() == View.GONE) {
                    list_partage.setVisibility(View.VISIBLE);
                    arrow_partage.setImageResource(R.drawable.ic_show_arrow_bottom);
                }
                else {
                    list_partage.setVisibility(View.GONE);
                    arrow_partage.setImageResource(R.drawable.ic_show_arrow);
                }
            }
        });
    }
}

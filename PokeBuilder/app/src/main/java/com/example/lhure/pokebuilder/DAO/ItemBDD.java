package com.example.lhure.pokebuilder.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.lhure.pokebuilder.Entity.Objet;

/**
 * Created by space on 07/01/2018.
 */

public class ItemBDD extends DAOBase {

/////////////////////////// Constructeurs ///////////////////////
    public ItemBDD(Context ctxt){
       super(ctxt);
    }

    public long insertItem(Objet o){
        ContentValues values= new ContentValues();
        //Add values
        values.put(DatabaseHandler.ITEM_NOM, o.getName());
        Log.e("Async task debug","Début de l'insert en base");
        return db.insert(DatabaseHandler.ITEM, null, values);
    }
    public long updateItem(Objet o){
        ContentValues values = new ContentValues();
        //values.put(COL_ID,i.getId());
        values.put(DatabaseHandler.ITEM_NOM, o.getName());
        return db.update(DatabaseHandler.ITEM, values, DatabaseHandler.ID+" = "+o.getId(), null);
    }
    public long removeItem(Integer id){
        return db.delete(DatabaseHandler.ITEM,DatabaseHandler.ID + " = "+id, null);
    }
}

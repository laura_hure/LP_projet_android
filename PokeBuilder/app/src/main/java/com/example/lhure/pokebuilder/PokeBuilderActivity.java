package com.example.lhure.pokebuilder;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.lhure.pokebuilder.DAO.AtkBDD;
import com.example.lhure.pokebuilder.DAO.DatabaseHandler;
import com.example.lhure.pokebuilder.DAO.EVBDD;
import com.example.lhure.pokebuilder.DAO.EquipeBDD;
import com.example.lhure.pokebuilder.DAO.ItemBDD;
import com.example.lhure.pokebuilder.DAO.PkmnBDD;
import com.example.lhure.pokebuilder.DAO.SetsBDD;
import com.example.lhure.pokebuilder.Entity.Attaque;
import com.example.lhure.pokebuilder.Entity.Set;
import com.example.lhure.pokebuilder.options.SettingsActivity;
import com.example.lhure.pokebuilder.teambuilder.objets.ObjetFragment;
import com.example.lhure.pokebuilder.teambuilder.search.SearchSetFragment;
import com.example.lhure.pokebuilder.teambuilder.search.TierChoiceDialog;
import com.example.lhure.pokebuilder.teambuilder.team.EquipeAddDialog;
import com.example.lhure.pokebuilder.teambuilder.team.EquipeChoiceDialog;
import com.example.lhure.pokebuilder.teambuilder.team.EquipeDeleteDialog;
import com.example.lhure.pokebuilder.teambuilder.team.EquipesFragment;
import com.example.lhure.pokebuilder.utils.NetworkChangeReceiver;
import com.example.lhure.pokebuilder.utils.NoticeDialogListener;

public class PokeBuilderActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, NoticeDialogListener {

    private SearchSetFragment searchSetFragment;    //fragment de la liste de pokemon
    private EquipesFragment equipesFragment;        //affichage des equipes
    private ObjetFragment objetFragment;            //Affichage des Objets (Poke API)
    private FloatingActionButton fab;               //button d'ajout des equipes
    private Boolean SHOW_SEARCH; //flag qui permet de savoir si on affiche ou non l'icone pour l'action search
    private IntentFilter networkFilter;
    private BroadcastReceiver networkChange;

    /**
     * catch les retours des boites de dialog
     * @param dialog
     * @param checkItem
     * @param itemId
     * @param item
     */
    @Override
    public void onDialogPositiveClick(DialogFragment dialog, String checkItem, long itemId, Object item) {
        //si le tier a ete choisi on le donne au fragment pour afficher les tiers
        if (dialog instanceof TierChoiceDialog)
            startSearchSetFragment(checkItem);

        if (dialog instanceof EquipeAddDialog)
            initEquipesFragment();

        //retour du choix de l'equipe a qui l'on va ajouter un set
        if (dialog instanceof EquipeChoiceDialog && item != null && item instanceof Set)
            addSetToEquipe(checkItem,itemId, (Set) item);

        if(dialog instanceof EquipeDeleteDialog)
            deleteEquipe(itemId);
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        handleIntent(getIntent()); // verifie si une recherche a ete effectue

        //register du receiver pour savoir si l'utilisateur a la connexion ou non
        networkFilter = new IntentFilter();
        networkFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        networkChange = new NetworkChangeReceiver();
        registerReceiver(networkChange,networkFilter);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null); //suppression des modification des couleurs
        fab = findViewById(R.id.fab);

        //si on est pas connecté on desactive le bouton "Set stratégique"
        if(!isOnline())
            navigationView.getMenu().getItem(1).setEnabled(false);
        //--------------------AJOUT DES EQUIPES----------------------------------------
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initEquipeAddFragment();
            }
        });

        //-----------------EQUIPES POKEMON-----------------------------------------------

        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById(R.id.fragment_container) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }
            //on rend visible le boutton d'ajout des equipes
            fab.setVisibility(View.VISIBLE);
            //initialisation du fragment
            initEquipesFragment();
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        if(SHOW_SEARCH){
            menu.findItem(R.id.action_search).setVisible(true);
        }else{
            menu.findItem(R.id.action_search).setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            //ouverture des options
            initOptions();
        }
        else if(id == R.id.action_search) {
            onSearchRequested();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_set_search) {
            //on cache le boutton d'ajout des equipes
            fab.setVisibility(View.GONE);
            //lancement de la fenêtre de dialog pour le choix du tier
            startTierChoiceDialog();

            //on start le fragment avec le recyclerview et les cards
            //startSearchSetFragment();
        } else if (id == R.id.nav_equipes) {
            //on rend visible le boutton d'ajout d'une équipe
            fab.setVisibility(View.VISIBLE);
            //affichage des equipe_card des utilisateurs
            initEquipesFragment();
        }else if(id == R.id.nav_item){
            fab.setVisibility(View.GONE);    //bouton d'ajout d'équipe invisible
            initItemFragment("",this);                 //affichage des Objets
        } else if (id == R.id.nav_share) {
            //envois du mail avec le contenu de la base
            String content = getMailContent();
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            composeEmail(new String[]{sharedPreferences.getString(getString(R.string.email),getString(R.string.email_default))},getString(R.string.subject),content);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * Lance le fragment des Items
     */
    private void initItemFragment(String query,Context context) {
        //affichage de l'icone de recherche de l'action bar
        SHOW_SEARCH=true;
        getSupportActionBar().setTitle(R.string.objets);
        invalidateOptionsMenu(); //recréer l'action bar

        // Create a new Fragment to be placed in the activity layout
        objetFragment = new ObjetFragment();

        //si il y a une requete passage de cette derniere au fragment
        if(!query.isEmpty()){
            Bundle bundle = new Bundle();
            bundle.putString("query",query);
            objetFragment.setArguments(bundle);
        }

        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment_container,objetFragment,"Objets");
        transaction.commit();
    }

    /**
     * lance le fragment de recherche de set avant le parametre de recherche
     *
     * @param item
     */
    private void startSearchSetFragment(String item) {
        SHOW_SEARCH=false;
        getSupportActionBar().setTitle(R.string.set);
        invalidateOptionsMenu();

        //Creation du bundle qui va contenir l'item selectionne par l'utilisateur
        Bundle bundle = new Bundle();
        bundle.putString("tier", item);

        // Handle the camera action
        // Create a new Fragment to be placed in the activity layout
        searchSetFragment = new SearchSetFragment();
        searchSetFragment.setArguments(bundle);

        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment_container, searchSetFragment, "set");
        transaction.commit();
    }

    /**
     * lance la fenêtre de dialog
     */
    private void startTierChoiceDialog() {
        DialogFragment newFragment = new TierChoiceDialog();
        newFragment.show(getSupportFragmentManager(), "tiers");
    }

    private void initEquipesFragment() {
        SHOW_SEARCH=false;
        getSupportActionBar().setTitle(R.string.equipe);
        invalidateOptionsMenu();

        // Handle the camera action
        // Create a new Fragment to be placed in the activity layout
        equipesFragment = new EquipesFragment();

        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment_container, equipesFragment, "equipes");
        transaction.commit();
    }

    /**
     * initialise la pop-up d'ajout d'une équipe
     */
    private void initEquipeAddFragment() {
        EquipeAddDialog equipeAddDialog = new EquipeAddDialog();
        equipeAddDialog.show(getSupportFragmentManager(), "ajouter_equipe");
    }

    /**
     * ajoute le set e l"equipe selectionne par l'utilisateur
     *
     * @param equipeId
     */
    private void addSetToEquipe(String checkItem, long equipeId, Set set) {
        EquipeBDD equipeBDD = new EquipeBDD(this);
        equipeBDD.open();

        //verification du nombre de set dans l'equipe
        //int nbSets = equipeBDD.getNbSets(equipeId);
        //if (nbSets < 6) {
            //ajout de l'item
            ItemBDD itemBDD = new ItemBDD(this);
            itemBDD.open();
            set.getObjet().setId(itemBDD.insertItem(set.getObjet()));
            itemBDD.close();

            //ajout des ev
            EVBDD evbdd = new EVBDD(this);
            evbdd.open();
            set.getEV().setId(evbdd.insertEv(set.getEV()));
            evbdd.close();

            //ajout du pokemon
            PkmnBDD pkmnBDD = new PkmnBDD(this);
            pkmnBDD.open();
            set.getPokemon().setId(pkmnBDD.insertPkmn(set.getPokemon()));
            pkmnBDD.close();

            //insertion des attaques
            AtkBDD atkBDD = new AtkBDD(this);
            atkBDD.open();
            for (Attaque attaque : set.getAttaques()) {
                attaque.setId(atkBDD.insertAtk(attaque));
            }
            atkBDD.close();

            //creation du set
            SetsBDD setsBDD = new SetsBDD(this);
            setsBDD.open();
            set.setId(setsBDD.insertSet(set));
            setsBDD.close();

            //Ajout du set à l'equipe
            equipeBDD.updateEquipe(equipeId, set, Integer.parseInt(checkItem)+1);

            //confirmation de l'ajout à l'utilisateur
            Toast.makeText(this,"Ajout du set réussi",Toast.LENGTH_SHORT).show();
        /*}else{
            Toast.makeText(this,"Cette équipe est pleine",Toast.LENGTH_SHORT).show();
        }*/
        //dans tout les cas on close la base
        equipeBDD.close();
    }

    /**
     * prend en entre l'id de l'equipe a supprimer et le supprimer avec les sets qui lui sont associe
     * @param id
     */
    private void deleteEquipe(long id){
        //ouverture de la base Equipe et recuperation de l'equipe a supprimer
        EquipeBDD equipeBDD = new EquipeBDD(this);
        equipeBDD.open();
        Cursor equipes = equipeBDD.getEquipeById(id);

        //ouverture de la base des sets
        SetsBDD setsBDD = new SetsBDD(this);
        setsBDD.open();

        //parcours des sets de l'equipe
        while(equipes.moveToNext()) {
            //recuperation des sets de l'equipe
            for (int i = 0; i < equipeBDD.getNbSets(id); i++) {
                //si l'equipe existe on prend son id
                if(!equipes.isNull(1+2)){
                    setsBDD.removeSet(equipes.getLong(i+2));
                }
            }
        }
        setsBDD.close();

        //suppression de l'equipe
        equipeBDD.removeEquipe(id);
        equipeBDD.close();

        //on refresh la vue
        initEquipesFragment();
    }

    /**
     * retourne si l'utilisateur est connecte ou non
     * @return boolean
     */
    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public void initOptions(){
       Intent intent = new Intent(this, SettingsActivity.class);
       startActivity(intent);
    }

    /**
     * recupere les informations dans la base pour envoyer l'email
     * @return
     */
    private String getMailContent(){
        String content = ""; //initialisation du contenu du mail
        //ouverture de la base des equipes
        EquipeBDD equipeBDD = new EquipeBDD(this);
        equipeBDD.open();
        Cursor equipes = equipeBDD.getAll(); //recuperation de toutes les equipes

        SetsBDD setsBDD = new SetsBDD(this); //ouverture de la bases des sets
        setsBDD.open();
        while(equipes.moveToNext()) {
            //ajout du nom de l'equipe au contenu du mail
            content += "EQUIPE : " + equipes.getString(equipes.getColumnIndex(DatabaseHandler.EQUIPE_NOM)) + "\n";
            //on parcours la liste des sets de l'equipe
           for(int i=2;i<equipeBDD.getNbSets(equipes.getLong(0))+2;i++){
                Cursor set = setsBDD.getSetById(equipes.getLong(i)); //recuperation du set avec son id

                //parcours du contenu du set
                while(set.moveToNext()) {
                    //ajout du contenu du set eu contenu du mail
                    content += set.getString(set.getColumnIndex(DatabaseHandler.SETS_EXPORT)) + "\n";
                }
           }
        }
        //fermeture du lien avec la base
        equipeBDD.close();
        setsBDD.close();

        //retour du contenu du mail
        return content;
    }

    /**
     * compose et un email et l'envois
     * @param addresses
     * @param subject
     * @param content
     */
    private void composeEmail(String[] addresses, String subject,String content) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, addresses); //adresse destination du mail
        intent.putExtra(Intent.EXTRA_SUBJECT, subject); //sujet du mail
        intent.putExtra(Intent.EXTRA_TEXT,content); // contenu du mail
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent); //envois du mail
        }
    }

    /**
     * permet de gérer plus proprement le cycle de vie de l'activité lors d'une recherche
     * @param intent
     */
    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    /**
     * cette methode est appele lorsqu'une recherche est effectue
     * @param intent
     */
    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            initItemFragment(query,this);
        }
    }
    /**
     * l'application entre dans cet état lorsque l'utilisateur utilise l'application
     */
    @Override
    public void onResume() {
        super.onResume();

        if(networkChange == null){
            if(networkFilter == null) {
                networkFilter = new IntentFilter();
                networkFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            }
            networkChange = new NetworkChangeReceiver();
        }
        registerReceiver(networkChange,networkFilter);
    }

    /**
     * L'application entre dans cet état lorsque l'utilisateur ne l'utilise plus
     */
    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(networkChange);
    }
}


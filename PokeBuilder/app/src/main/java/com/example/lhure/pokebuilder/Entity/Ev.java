package com.example.lhure.pokebuilder.Entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Laura on 13/01/2018.
 */

public class Ev implements Parcelable {

    private String Pv, Att, Def, SAtt, SDef, Vit;
    private long id;

    public Ev(String Pv, String Att, String Def, String SAtt, String SDef, String Vit){
        this.Pv = Pv;
        this.Att = Att;
        this.Def = Def;
        this.SAtt = SAtt;
        this.SDef = SDef;
        this.Vit = Vit;
    }

    public String getPv(){return this.Pv;}
    public String getAtt(){return this.Att;}
    public String getDef(){return this.Def;}
    public String getSAtt(){return this.SAtt;}
    public String getSDef(){return this.SDef;}
    public String getVit(){return this.Vit;}
    public long getId(){return this.id;}
    public void setId(long id){this.id=id;}

    /* Factory Creator */
    public static final Creator<Ev> CREATOR = new Creator<Ev>() {
        @Override
        public Ev createFromParcel(Parcel in) {
            return new Ev(in);
        }

        @Override
        public Ev[] newArray(int size) {
            return new Ev[size];
        }
    };

    private Ev(Parcel in){
        this.id = in.readLong();

        this.Pv = in.readString();
        this.Att = in.readString();
        this.Def = in.readString();
        this.SAtt = in.readString();
        this.SDef = in.readString();
        this.Vit = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(Pv);
        dest.writeString(Att);
        dest.writeString(Def);
        dest.writeString(SAtt);
        dest.writeString(SDef);
        dest.writeString(Vit);
    }
}

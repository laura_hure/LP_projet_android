package com.example.lhure.pokebuilder.Entity;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Laura on 12/01/2018.
 */

public class Objet implements Parcelable{

    private String name;
    private long id;
    private String description;


    public Objet(String name){
        this.name = name;
    }

    public long getId(){return this.id;}
    public void setId(long id){this.id = id;}
    public String getName(){return this.name;}

    /* Factory Creator */
    public static final Creator<Objet> CREATOR = new Creator<Objet>() {
        @Override
        public Objet createFromParcel(Parcel in) {
            return new Objet(in);
        }

        @Override
        public Objet[] newArray(int size) {
            return new Objet[size];
        }
    };

    private Objet(Parcel in){
        this.id = in.readLong();
        this.name = in.readString();
        this.description = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(description);
    }
    public String getDescription(){
        if (this.description!=null){
            return this.description;
        }else{
            return "";
        }
    }

    public void setDescription(String desc){
        this.description = desc;
    }

    public String toString(){
        return this.name +": " + this.description;
    }
}
